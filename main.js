import Vue from 'vue'
import App from './App'
import store from './store'
import uView from "uview-ui";

import i18n from './lang'
Vue.prototype._i18n = i18n
Vue.use(uView)

import Upload from './components/upload/index.vue'
import videoUpload from './components/upload/video.vue'
Vue.component('Upload', Upload)
Vue.component('videoUpload', videoUpload)
Vue.config.productionTip = false
// Vue.prototype.fileUrl = 'https://pre-cg-iot.yunzhen.tech'
Vue.prototype.fileUrl = 'https://iotplat.yunzhen.tech:8083'
// Vue.prototype.fileUrl = 'http://localhost:8080/'

App.mpType = 'app'

const app = new Vue({
  store,
  i18n,
  ...App
})
app.$mount()
