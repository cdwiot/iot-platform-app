const CONFIG = require("@/config.js");
const API_BASE_URL = CONFIG.domin;

const request = (url, needSubDomain, method, data) => {

	let _url = API_BASE_URL + (needSubDomain ? "/" + CONFIG.subDomain : "") + url;
	let token = uni.getStorageSync('token')
	let header
	if (url === '/login') {
		header = {
			"Content-Type": "application/json"
		}
	} else {
		header = {
			"Content-Type": "application/json",
			"Authorization": 'Bearer ' + token
		}
	}
	return new Promise((resolve, reject) => {
		uni.request({
			url: _url,
			method: method,
			data: data,
			header: header,
			success(response) {
				const code = response.data.code;
				// 通过statusCode 统一处理，可根据具体情况修改
				if (code === 200) {
					resolve(response.data);
				} else if (code == 401) {
					uni.showToast({
						title: "登录过期",
						icon: "none",
						duration: 2000
					});
					uni.redirectTo({
						url: "/pages/login/login"
					});

					uni.removeStorageSync("username");
					uni.removeStorageSync("password");
					uni.removeStorageSync("rember");
					uni.removeStorageSync("nickName");
					uni.removeStorageSync("current");
					reject(response.data);
				} else if (code === 500) {
					uni.showToast({
						title: response.data.msg,
						icon: "none"
					});
					reject(response.data);
				} else if (code === 403) {
					uni.showToast({
						title: response.data.msg,
						icon: "none"
					});
					reject(response.data);
				} else {
					resolve(response.data);
				}
			},
			fail(error) {
				console.log(error, 'xz')
				uni.showToast({
					title: error.errMsg,
					icon: "none"
				});
				reject(error);
			}
		});
	});
};

module.exports = {
	request,
	// login
	login: data => {
		return request("/login", true, "post", data);
	},
	// 点检任务首页查询
	pointCheckTaskList: data => {
		return request("/system/point_check_task/app/list", true, "get", data);
	},
	// 点检任务进行中查询
	pointCheckTaskListProgress: data => {
		return request("/system/point_check_task/app/list_progress", true, "get", data);
	},
	// 点检任务设备查询
	pointCheckTaskDevice: data => {
		return request("/system/point_check_task_device/list", true, "get", data);
	},
	// 点检任务绑定设备
	pointCheckTaskDeviceAdd: data => {
		return request("/system/point_check_task_device/add", true, "post", data);
	},
	// 点检任务修改设备
	pointCheckTaskDeviceEdit: data => {
		return request("/system/point_check_task_device/edit", true, "post", data);
	},
	// 点检任务解绑设备
	pointCheckTaskDeviceRemove: id => {
		return request("/system/point_check_task_device/" + id, true, "delete");
	},

	// 查询检查项信息
	devicePointCheckConfig: taskDeviceId => {
		return request("/system/device/point/check/config/default/enum/" + taskDeviceId, "get", );
	},
	// 设备检查信息
	pointCheckDeviceDetail: taskDeviceId => {
		return request("/system/point_check_device_detail/get_device_detailVO?taskDeviceId=" + taskDeviceId, "get", );
	},
	pointCheckDeviceDetail1: data => {
		return request("/system/point_check_device_detail/get_device_detailVO", true, "get", data);
	},
	pointCheckDeviceDdescription: id => {
		return request("/system/point_check_description/query_description?detailId=" + id, "get");
	},
	pointCheckDescriptiont: data => {
		return request("/system/point_check_description/update_batch", true, "post", data);
	},
	// 设备添加信息
	pointCheckDeviceDetailAdd: data => {
		return request("/system/point_check_device_detail/add_update", true, "post", data);
	},
	// 设备检查修改信息-----------------------
	pointCheckDeviceDetai: id => {
		return request("/system/point_check_device_detail/get_info?id=" + id, "get", );
	},
	// 设备修改信息
	pointCheckDeviceDetailEdit: data => {
		return request("/system/point_check_device_detail/edit", true, "post", data);
	},
	// 文字信息查询
	pointCheckeDviceDetailList: detailId => {
		return request("/system/point_check_description/query_description?detailId=" + detailId, "get", );
	},
	// 文字添加信息
	pointCheckDescriptionAdd: data => {
		return request("/system/point_check_description/add_update", true, "post", data);
	},
	//  文字修改信息
	pointCheckDescriptionEdit: data => {
		return request("/system/point_check_description/edit", true, "post", data);
	},
	// 处理方式信息查询
	pointCheckeTaskDetailList: detailId => {
		return request("/system/point_check_task_deal/query_deal?detailId=" + detailId, "get", );
	},
	// 处理方式添加信息
	pointCheckeTaskDetailAdd: data => {
		return request("/system/point_check_task_deal/add_update", true, "post", data);
	},
	//一键生成
	pointCheckTaskDeviceQuickCreateReport: id => {
		return request("/system/point_check_task_device/quick_create_report?id=" + id, "get", );
	},
	//生成报告
	pointCheckTaskDeviceCreateReport: data => {
		return request("/system/point_check_task_device/create_report_v2", true, "get", data);
	},
	//用户名
	userProfileget: () => {
		return request("/system/user/profile", "get", );
	},
	//获取图片
	productProperty: id => {
		return request("/system/product/property/value/productId?productId=" + id, "get", );
	},
	// 重置密码
	updateUserPwd: (oldPassword, newPassword) => {
		return request("/system/user/profile/updatePwd?oldPassword=" + oldPassword + "&newPassword=" + newPassword, true, 'put', );
	},
	//汇总表
	createTaskGather: id => {
		return request("/system/point_check_task/create_task_gather?id=" + id, "get", );
	},
	//产品运行汇总表
	stateGather: () => {
		return request("/system/report_detail/device_status_summary", "get", );
	},
	//销售汇总表
	saleGather: () => {
		return request("/system/report_detail/sales_summary", "get", );
	},
	//检查类别查询
	pointCheckConfig: data => {
		return request("/system/product/point/check/config/list", true, "get", data);
	},
	//检查项具体内容
	pointCheckDeviceDetailList: id => {
		return request("/system/point_check_device_detail/list?taskDeviceId=" + id, "get", );
	},

	logout: () => {
		return request("/user/login", true, "post", {});
	},
	// user
	userInfo: () => {
		return request("/user/info", true, "post", {});
	},
	userList: (page_no, page_size, data) => {
		return request(
			"/user/list?page_no=" + page_no + "&page_size=" + page_size,
			true,
			"post",
			data
		);
	},
	retrieveUserById: id => {
		return request("/user/retrieve/" + id, true, "post", {});
	},
	updateUserInfo: (id, data) => {
		return request("/user/update/" + id, true, "post", data);
	},
	// device
	deviceList: (page_no, page_size, data) => {
		return request(
			"/device/list?page_no=" + page_no + "&page_size=" + page_size,
			true,
			"post",
			data
		);
	},
	deviceDetail: id => {
		return request("/device/retrieve/" + id, true, "post", {});
	},
	// alarm
	alarmList: (page_no, page_size, data) => {
		return request(
			"/alarm/list?page_no=" + page_no + "&page_size=" + page_size,
			true,
			"post",
			data
		);
	},
	// analysis
	historyData: data => {
		return request("/history/list", true, "post", data);
	},
	// profile
	userProfile: () => {
		return request("/user/retrieve", true, "post", {});
	},
	userProfileUpdate: data => {
		return request("/user/update", true, "post", data);
	},
	//common
	locationList: () => {
		return request("/location/list", true, "post", {});
	},
	//安装指导列表
	installGuideList: data => {
		return request("/system/installation_guide/list", true, "get", data);
	},
	dataQuery: (data, code) => {
		return request("/system/regulated_data/query?code=" + code, true, "post", data);
	},
	// 产品异常状况
	deviceHistory: data => {
		return request("/system/point_check_task/get_device_history", true, "get", data);
	},
	// 报警列表
	monitorlist: data => {
		return request("/system/alarm/list", true, "get", data);
	},
	// 报警列表详情
	eventDetail: id => {
		return request("/system/alarm/" + id, true, "get");
	},
	// 懒人产品扫描
	deviceDeail: data => {
		return request("/system/point_check_task_device/get_device_detail", true, "get", data);
	},
	// 点检配置查询
	checkQueryConfig: data => {
		return request("/system/device/point/check/config/default/enum_v2", true, "get", data);
	},
};