### [v2.12.26](https://github.com/youzan/vant/compare/v2.12.25...v2.12.26)

`2021-08-15`

**Bug Fixes**

- cli: fix webpackpack config value duplication caused by origin merge [#9221](https://github.com/youzan/vant/issues/9221)
- Field: textarea scroll to top after resizing [#9207](https://github.com/youzan/vant/issues/9207)
- markdown-loader: windows path format [#9127](https://github.com/youzan/vant/issues/9127) [#9128](https://github.com/youzan/vant/issues/9128)
- Rate: should enable flex wrap [#9193](https://github.com/youzan/vant/issues/9193)
- Tabs: remove invalid head padding for card type [#9169](https://github.com/youzan/vant/issues/9169)

**Document**

- @vant/cli: changelog 2.11.3 [4ff443](https://github.com/youzan/vant/commit/4ff44356fa58388b572633c9516cbfe5c4a485b4)
- changelog: 2.12.25 [4a73da](https://github.com/youzan/vant/commit/4a73da768f7979c8212ef62f43762272095f9eaf)

**Feature**

- Uploader: add click-upload event [#9260](https://github.com/youzan/vant/issues/9260)
- Uploader: add readonly prop [#9257](https://github.com/youzan/vant/issues/9257)

**release**

- @vant/markdown-loader@2.5.1 [78712e](https://github.com/youzan/vant/commit/78712e135d304b35514c81e192e8410c67a226d5)
