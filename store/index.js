import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        hasLogin: false,
        userName: ""
    },
    mutations: {
        SET_LOGIN(state, status) {
            state.hasLogin = status;
        },
        SET_USERNAME(state, name) {
            state.userName = name;
        }
    },
	actions: {
	}
})

export default store
